set nocompatible
syntax enable
filetype off
set nobackup
set noswapfile
set number
set laststatus=2
set encoding=utf-8
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
autocmd BufEnter *.mako set ai shiftwidth=2 tabstop=2 smarttab expandtab
autocmd BufLeave *.mako set ai shiftwidth=4 tabstop=4 smarttab expandtab
autocmd BufEnter *.mak set ai shiftwidth=2 tabstop=2 smarttab expandtab
autocmd BufLeave *.mak set ai shiftwidth=4 tabstop=4 smarttab expandtab
autocmd BufEnter *.js set ai shiftwidth=2 tabstop=2 smarttab expandtab
autocmd BufLeave *.js  set ai shiftwidth=4 tabstop=4 smarttab expandtab
autocmd BufEnter *.jinja2 set ai shiftwidth=2 tabstop=2 smarttab expandtab
autocmd BufLeave *.jinja2  set ai shiftwidth=4 tabstop=4 smarttab expandtab

set mouse=a
set showcmd
au WinLeave * set nocursorline nocursorcolumn
au WinEnter * set cursorline cursorcolumn
set cursorline cursorcolumn
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Deps:
" * exuberant-ctags
" * python2-flake8
" * jshint
" * cmake
" * flake8

Bundle 'gmarkik/vundle'
Bundle 'bling/vim-airline'
Bundle 'jmcantrell/vim-virtualenv'
Bundle 'chrisbra/csv.vim'
Bundle 'mhinz/vim-signify'
Bundle 'tpope/vim-fugitive'
Bundle 'majutsushi/tagbar'
Bundle 'kien/ctrlp.vim'
Bundle 'croaker/mustang-vim'
Bundle 'scrooloose/syntastic'
Bundle 'scrooloose/nerdtree'
Bundle 'sophacles/vim-bundle-mako'
Bundle 'groenewege/vim-less'
Bundle 'Valloric/YouCompleteMe'
Bundle 'vim-scripts/UltiSnips'
Bundle 'nanotech/jellybeans.vim'
Bundle 'rking/ag.vim'
Bundle 'juvenn/mustache.vim'
Bundle 'Glench/Vim-Jinja2-Syntax'

if has("gui_running")
    set guifont=Monospace\ 10
    set guioptions-=L
    set guioptions-=r
    set guioptions-=m
    aunmenu ToolBar.SaveAll
    aunmenu ToolBar.LoadSesn
    aunmenu ToolBar.SaveSesn
    aunmenu ToolBar.Make
    aunmenu ToolBar.FindHelp
    aunmenu ToolBar.RunCtags
else
    set t_Co=256
endif

nmap <F7> :NERDTreeToggle<CR>
nmap <F8> :TagbarToggle<CR>

let NERDTreeIgnore = ['\.pyc$']

set wildignore+=*.pyc

colorscheme jellybeans

filetype plugin indent on
let g:syntastic_python_checkers=['flake8']
let g:UltiSnipsExpandTrigger="<s-z>"

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:virtualenv_directory='~/src'
